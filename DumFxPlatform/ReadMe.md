Java Coding Test
Imagine you're a developer working for a company that trades currencies and you have been given the task to implement a simplified version of a FX trading platform called “Dummy FX platform”.
“Dummy FX platform” is a Java/Spring Service exposing a REST API. The requirements for phase 1 are limited in scope, not well refined and need to be interpreted by the Developer who will make his assumptions to implement the best possible lean solution.
The Service should provide the following functionalities:
1) For this simple version the current price is static and it is supplied via config file (E.g.
GBP/USD = 1.2100).
2) Order registration.
-The order contains fields → userId, orderType: BID or ASK, currency(GBP/USD), price(E.g. 1.2100), amount(E.g. 8500).
3) Cancel an order.
4) Return summary regarding live not matched orders.
5) Return a summary of matched trades.
Limitations of “Dummy FX platform” for phase 1:
- No permanent storage required. Implement simple runtime memory storage.
- “Dummy FX platform” works only for GBP/USD trading.
Non functional requirements:
-Ensure enough test coverage.
-Make sure the the solution is easy to run via a build tool (Maven or Gradle).



