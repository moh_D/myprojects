package com.worldfirst.demo;


import com.worldfirst.demo.model.Order;
import com.worldfirst.demo.model.OrderType;
import com.worldfirst.demo.service.OrderServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class OrderServiceImplTest {

    private OrderServiceImpl orderService;

    @Before
    public void setUp() {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void addLiveOrder_nullInput_returnEnptyList() {
        orderService.addLiveOrder(null);
        Assert.assertTrue(orderService.getLiveOrders().isEmpty());
    }

    @Test
    public void addLiveOrder_validInput_ExpectedCorrectResult() {
        orderService.addLiveOrder(createASKOrder());
        Assert.assertFalse(orderService.getLiveOrders().isEmpty());
    }

    @Test
    public void addLiveOrder_validInput_OrderIsSaved() {
        Order order = createASKOrder();
        orderService.addLiveOrder(createASKOrder());
        Assert.assertTrue(orderService.getLiveOrders().get(order.getOrderType()).contains(order));
    }

    @Test
    public void addLiveOrder_validInputASK_OrderIsSavedInASKMap() {
        Order order = createASKOrder();
        orderService.addLiveOrder(createASKOrder());
        Assert.assertTrue(orderService.getLiveOrdersByType(OrderType.ASK).contains(order));
        Assert.assertNull(orderService.getLiveOrdersByType(OrderType.BID));
    }

    @Test
    public void addLiveOrder_validInputBID_OrderIsSavedInBIDMap() {
        Order order = createBIDOrder();
        orderService.addLiveOrder(order);
        Assert.assertNull(orderService.getLiveOrdersByType(OrderType.ASK));
        Assert.assertTrue(orderService.getLiveOrdersByType(OrderType.BID).contains(order));
    }

    @Test
    public void addLiveOrder_AskOrderMatchedWithBidOrder_LiveOrdersListIsEmpty() {
        Order askOrder = createASKOrder();
        Order bidOrder = createBIDMatchingASK();
        orderService.addLiveOrder(askOrder);
        orderService.addLiveOrder(bidOrder);
        Assert.assertEquals(0, orderService.getLiveOrdersByType(OrderType.ASK).size());
        Assert.assertNull(orderService.getLiveOrdersByType(OrderType.BID));
        Assert.assertEquals(1 , orderService.getMatchedOrders().size());
    }

    @Test
    public void addLiveOrder_BidOrderMatchedWithAskOrder_LiveOrdersListIsEmpty() {
        Order bidOrder = createBIDOrder();
        Order askOrder = createASKMatchingBID();
        orderService.addLiveOrder(bidOrder);
        orderService.addLiveOrder(askOrder);
        Assert.assertEquals(0, orderService.getLiveOrdersByType(OrderType.BID).size());
        Assert.assertNull(orderService.getLiveOrdersByType(OrderType.ASK));
        Assert.assertEquals(1 , orderService.getMatchedOrders().size());
    }

    @Test
    public void cancelOrder_nullInput_ExpectedCorrectResult() {
        orderService.cancelLiveOrder(null);
        Assert.assertTrue(orderService.getLiveOrders().isEmpty());
    }

    @Test
    public void cancelOrder_ValidInput_returnEmptyList() {
        Order order = createASKOrder();
        orderService.addLiveOrder(order);

        orderService.cancelLiveOrder(order);
        Assert.assertTrue(orderService.getLiveOrdersByType(OrderType.ASK).isEmpty());
    }

    @Test
    public void cancelOrder_ValidInput_returnASKEmptyList() {
        Order askOrder = createASKOrder();
        Order bidOrder = createBIDOrder();
        orderService.addLiveOrder(askOrder);
        orderService.addLiveOrder(bidOrder);

        orderService.cancelLiveOrder(askOrder);
        Assert.assertTrue(orderService.getLiveOrdersByType(OrderType.ASK).isEmpty());
        Assert.assertFalse(orderService.getLiveOrdersByType(OrderType.BID).isEmpty());
    }

    @Test
    public void cancelOrder_ValidInput_returnBIDEmptyList() {
        Order askOrder = createASKOrder();
        Order bidOrder = createBIDOrder();
        orderService.addLiveOrder(askOrder);
        orderService.addLiveOrder(bidOrder);

        orderService.cancelLiveOrder(askOrder);
        Assert.assertFalse(orderService.getLiveOrdersByType(OrderType.BID).isEmpty());
        Assert.assertTrue(orderService.getLiveOrdersByType(OrderType.ASK).isEmpty());
    }

    @Test
    public void cancelOrder_ValidInput_returnASKAndBIDEmptyList() {
        Order askOrder = createASKOrder();
        Order bidOrder = createBIDOrder();
        orderService.addLiveOrder(askOrder);
        orderService.addLiveOrder(bidOrder);

        orderService.cancelLiveOrder(askOrder);
        orderService.cancelLiveOrder(bidOrder);
        Assert.assertTrue(orderService.getLiveOrdersByType(OrderType.BID).isEmpty());
        Assert.assertTrue(orderService.getLiveOrdersByType(OrderType.ASK).isEmpty());
    }

    @Test
    public void getASKLiveOrders_nullInput() {
        orderService.addLiveOrder(null);
        Assert.assertNull( orderService.getLiveOrdersByType(OrderType.ASK));
    }

    @Test
    public void getASKLiveOrders_ValidInput_ExpectedBehavior() {
        orderService.addLiveOrder(createASKOrder());

        Assert.assertNull( orderService.getLiveOrdersByType(OrderType.BID));
        Assert.assertNotNull( orderService.getLiveOrdersByType(OrderType.ASK));
        Assert.assertEquals( 1, orderService.getLiveOrdersByType(OrderType.ASK).size());
    }

    @Test
    public void getASKLiveOrders_ValidInput_ValueIsSavec() {
        Order order = createASKOrder();
        orderService.addLiveOrder(createASKOrder());

        Assert.assertNull( orderService.getLiveOrdersByType(OrderType.BID));
        Assert.assertNotNull( orderService.getLiveOrdersByType(OrderType.ASK));
        Assert.assertEquals( order, orderService.getLiveOrdersByType(OrderType.ASK).get(0));
    }

    @Test
    public void getBIDLiveOrders_nullInput() {
        orderService.addLiveOrder(null);

        Assert.assertNull( orderService.getLiveOrdersByType(OrderType.BID));
        Assert.assertNull( orderService.getLiveOrdersByType(OrderType.ASK));
    }

    @Test
    public void getBIDLiveOrders_ValidInput_ExpectedBehavior() {
        orderService.addLiveOrder(createBIDOrder());

        Assert.assertNull( orderService.getLiveOrdersByType(OrderType.ASK));
        Assert.assertNotNull( orderService.getLiveOrdersByType(OrderType.BID));
        Assert.assertEquals( 1, orderService.getLiveOrdersByType(OrderType.BID).size());
    }

    @Test
    public void getBIDLiveOrders_ValidInput_ValueIsSavec() {
        Order order = createBIDOrder();
        orderService.addLiveOrder(order);

        Assert.assertNull( orderService.getLiveOrdersByType(OrderType.ASK));
        Assert.assertNotNull( orderService.getLiveOrdersByType(OrderType.BID));
        Assert.assertEquals( order, orderService.getLiveOrdersByType(OrderType.BID).get(0));
    }

    @Test
    public void getMatchedOrders_AddNullLiveOrder_ExpectedEmptyList() {
        orderService.addLiveOrder(null);
        Assert.assertTrue( orderService.getMatchedOrders().isEmpty());
    }

    @Test
    public void getMatchedOrders_AddValidLiveOrder_ExpectedBehavior() {
        orderService.addLiveOrder(createASKOrder());
        Assert.assertTrue( orderService.getMatchedOrders().isEmpty());
    }

    @Test
    public void getMatchedOrders_AddASKAndBIDValidLiveOrder_ExpectedBehavior() {
        orderService.addLiveOrder(createASKOrder());
        orderService.addLiveOrder(createBIDMatchingASK());

        Assert.assertFalse( orderService.getMatchedOrders().isEmpty());
        Assert.assertEquals(1, orderService.getMatchedOrders().size());
    }

    @Test
    public void getMatchedOrders_AddASKAndBIDValidLiveOrder_OrdersAreSavedInMatchedOrdersList() {
        Order askOrder = createASKOrder();
        Order bidOrder = createBIDMatchingASK();
        orderService.addLiveOrder(askOrder);
        orderService.addLiveOrder(bidOrder);

        Assert.assertFalse( orderService.getMatchedOrders().isEmpty());
        Assert.assertEquals(1, orderService.getMatchedOrders().size());
        Assert.assertEquals(askOrder, orderService.getMatchedOrders().get(0).getAskOrder());
        Assert.assertEquals(bidOrder, orderService.getMatchedOrders().get(0).getBidOrder());

    }

    private Order createASKOrder(){
        return new Order(1, OrderType.ASK, "GBP/USD", 1.2111, 4500);
    }

    private Order createBIDMatchingASK(){
        return new Order(1, OrderType.BID, "GBP/USD", 1.2111, 4500);
    }

    private Order createBIDOrder(){
        return new Order(1, OrderType.BID, "GBP/USD", 1.2111, 4600);
    }

    private Order createASKMatchingBID(){
        return new Order(1, OrderType.ASK, "GBP/USD", 1.2111, 4600);
    }



}
