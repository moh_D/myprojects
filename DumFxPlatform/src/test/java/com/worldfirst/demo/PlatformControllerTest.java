package com.worldfirst.demo;


import com.worldfirst.demo.controller.PlatformController;
import com.worldfirst.demo.dto.OrderDTO;
import com.worldfirst.demo.facade.OrderServiceFacade;
import com.worldfirst.demo.model.OrderType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PlatformController.class)
@AutoConfigureMockMvc
public class PlatformControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    OrderServiceFacade orderServiceFacadeMock;


    @Test
    public void shouldReturnBidOrders() throws Exception{

        // given
        List<OrderDTO> orderDTOS = Mockito.spy(new ArrayList<OrderDTO>());
        OrderDTO bidOrder = createBIDOrderDTO();
        orderDTOS.add(bidOrder);

        Mockito.verify(orderDTOS).add(bidOrder);

        // when
        when(orderServiceFacadeMock.getLiveOrdersByType(OrderType.BID)).thenReturn(orderDTOS);

        // then
        mockMvc.perform(get("/worldFirst/BID/liveOrders"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].orderType", is(OrderType.BID.name())))
                .andExpect(jsonPath("$[0].currency", is("GBP_USD")));

    }

    @Test
    public void shouldReturnAskOrders() throws Exception{

        // given
        List<OrderDTO> orderDTOS = new ArrayList<>();
        orderDTOS.add(createASKOrderDTO());

        // when
        when(orderServiceFacadeMock.getLiveOrdersByType(OrderType.ASK)).thenReturn(orderDTOS);

        // then
        mockMvc.perform(get("/worldFirst/ASK/liveOrders").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].orderType", is(OrderType.ASK.name())))
                .andExpect(jsonPath("$[0].currency", is("GBP_USD")))
                .andExpect(jsonPath("$[0].price", is(0.0)))
                .andExpect(jsonPath("$[0].amount", is(4500.0)));

    }

    private OrderDTO createASKOrderDTO() {
        return new OrderDTO(1, OrderType.ASK, "GBP_USD", 0.0, 4500);
    }

    private OrderDTO createBIDOrderDTO() {
        return new OrderDTO(1, OrderType.BID, "GBP_USD", 0.0, 4600);
    }

}
