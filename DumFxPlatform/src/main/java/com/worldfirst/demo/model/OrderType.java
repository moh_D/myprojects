package com.worldfirst.demo.model;

public enum OrderType {
    ASK, BID
}
