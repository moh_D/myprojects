package com.worldfirst.demo.model;

public class MatchedTrade {

    private Order askOrder;
    private Order bidOrder;

    public MatchedTrade(Order askOrder, Order bidOrder){
        this.askOrder = askOrder;
        this.bidOrder = bidOrder;
    }

    public Order getAskOrder() {
        return askOrder;
    }

    public void setAskOrder(Order askOrder) {
        this.askOrder = askOrder;
    }

    public Order getBidOrder() {
        return bidOrder;
    }

    public void setBidOrder(Order bidOrder) {
        this.bidOrder = bidOrder;
    }

    @Override
    public String toString() {
        return "MatchedTrade{" +
                "\naskOrder=" + askOrder +
                ", \nbidOrder=" + bidOrder +
                '}';
    }
}
