package com.worldfirst.demo.model;

import org.modelmapper.ModelMapper;


public class Order extends ModelMapper{

    private int id;
    private OrderType orderType;
    private String currency;
    private double amount;
    private double price;

    public Order(int id, OrderType orderType, String currency, double amount, double price) {
        this.id = id;
        this.orderType = orderType;
        this.currency = currency;
        this.amount = amount;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean matchedOrder(Order order){
        if (Double.compare(order.amount, amount) != 0) return false;
        if (Double.compare(order.price, price) != 0) return false;
        if (orderType == order.orderType) return false;
        return currency.equals(order.currency);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (id != order.id) return false;
        if (Double.compare(order.amount, amount) != 0) return false;
        if (Double.compare(order.price, price) != 0) return false;
        if (orderType != order.orderType) return false;
        return currency.equals(order.currency);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + orderType.hashCode();
        result = 31 * result + currency.hashCode();
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", orderType=" + orderType +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                ", price=" + price +
                '}';
    }
}
