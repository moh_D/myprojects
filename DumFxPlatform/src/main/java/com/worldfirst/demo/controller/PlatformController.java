package com.worldfirst.demo.controller;

import com.worldfirst.demo.facade.OrderServiceFacade;
import com.worldfirst.demo.dto.MatchedTradeDTO;
import com.worldfirst.demo.dto.OrderDTO;
import com.worldfirst.demo.model.OrderType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import javax.ws.rs.core.Response;


@Controller
@RequestMapping("/worldFirst")
@ComponentScan("com.worldfirst.demo")
public class PlatformController {

    @Autowired
    OrderServiceFacade orderServiceFacade;

    @RequestMapping(value="/createOrder", method = RequestMethod.POST)
    @ResponseBody
    public Response.Status createOrder(@RequestBody OrderDTO orderDTO) {

        Response.Status response;
        try{
            orderServiceFacade.addLiveOrder(orderDTO);
            response = Response.Status.CREATED;
        }catch(Exception e) {
            response = Response.Status.BAD_REQUEST;
        }

        return response ;
    }

    @RequestMapping(value="/{orderType}/liveOrders", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderDTO> getLiveOrdersByType(@PathVariable OrderType orderType) {

        return orderServiceFacade.getLiveOrdersByType(orderType);
    }

    @RequestMapping(value="/matchedOrders", method = RequestMethod.GET)
    @ResponseBody
    public List<MatchedTradeDTO> getMatchedOrders() {

        return orderServiceFacade.getMatchedOrders();
    }

    @RequestMapping(value="/cancelOrder", method = RequestMethod.DELETE)
    @ResponseBody
    public Response cancelLiveOrders(@RequestBody OrderDTO orderDTO) {

        Response response;
        try{
            orderServiceFacade.cancelLiveOrder(orderDTO);
            response = Response.status(Response.Status.OK).build();
        }catch(Exception e) {
            response = Response.status(Response.Status.NOT_MODIFIED).build();
        }

        return response ;
    }
}

