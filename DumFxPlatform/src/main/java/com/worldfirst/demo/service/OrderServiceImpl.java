package com.worldfirst.demo.service;

import com.worldfirst.demo.model.MatchedTrade;
import com.worldfirst.demo.model.Order;
import com.worldfirst.demo.model.OrderType;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {

    private Map<OrderType,List<Order>> liveOrders = new HashMap<>();
    private List<MatchedTrade> matchedOrders = new ArrayList<>();

    public void addLiveOrder(Order order){
        if(order != null){
            if(!liveOrders.containsKey(order.getOrderType())){
                if (!isMatchedOrder(order)) {
                    List<Order> orders = new ArrayList<>();
                    orders.add(order);
                    liveOrders.put(order.getOrderType(), orders);
                }

            } else {
                if(!isMatchedOrder(order)){
                    liveOrders.get(order.getOrderType()).add(order);
                }
            }
        }

    }

    public void cancelLiveOrder(Order order){
        if(order != null && order.getOrderType() != null){
            liveOrders.get(order.getOrderType()).remove(order);
        }
    }

    public Map<OrderType,List<Order>> getLiveOrders(){
        return liveOrders;
    }

    public List<Order> getLiveOrdersByType(OrderType orderType){
        return liveOrders.get(orderType);
    }

    public List<MatchedTrade> getMatchedOrders(){
        return matchedOrders;
    }


    private boolean isMatchedOrder(Order order){

        if(order == null){
            return false;
        }
        if(order.getOrderType().equals(OrderType.ASK) && liveOrders.get(OrderType.BID) != null){
            List<Order> orders = liveOrders.get(OrderType.BID);
            if (findMatchingOrder(order, orders)) return true;
        }
        if(order.getOrderType().equals(OrderType.BID) && liveOrders.get(OrderType.ASK) != null){
            List<Order> orders = liveOrders.get(OrderType.ASK);
            if(findMatchingOrder(order, orders)) return true;
        }

        return false;

    }

    private boolean findMatchingOrder(Order order, List<Order> orders) {
        for(int i = 0; i < orders.size(); i++){
            if(orders.get(i).matchedOrder(order)){
                if(order.getOrderType().equals(OrderType.BID)){
                    addMatchedTrade(orders.get(i), order);
                } else {
                    addMatchedTrade(order, orders.get(i));
                }
                cancelLiveOrder(orders.get(i));
                return true;
            }
        }
        return false;
    }

    private void addMatchedTrade(Order askOrder, Order bidOrder){
        if(askOrder.matchedOrder(bidOrder)){
            matchedOrders.add(new MatchedTrade(askOrder, bidOrder));
        }
    }
}
