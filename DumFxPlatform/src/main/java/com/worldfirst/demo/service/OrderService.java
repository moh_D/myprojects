package com.worldfirst.demo.service;

import com.worldfirst.demo.model.MatchedTrade;
import com.worldfirst.demo.model.Order;
import com.worldfirst.demo.model.OrderType;


import java.util.List;
import java.util.Map;

public interface OrderService {

    void addLiveOrder(Order order);
    void cancelLiveOrder(Order order);
    Map<OrderType,List<Order>> getLiveOrders();
    List<MatchedTrade> getMatchedOrders();
    List<Order> getLiveOrdersByType(OrderType orderType);
}
