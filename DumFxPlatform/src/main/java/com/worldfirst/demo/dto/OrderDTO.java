package com.worldfirst.demo.dto;

import com.worldfirst.demo.model.OrderType;

public class OrderDTO {

    private int id;
    private OrderType orderType;
    private String currency;
    private double price;
    private double amount;

    public OrderDTO(){
    }

    public OrderDTO(int id, OrderType orderType, String currency, double price, double amount) {
        this.id = id;
        this.orderType = orderType;
        this.currency = currency;
        this.price = price;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
