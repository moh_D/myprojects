package com.worldfirst.demo.dto;

public class MatchedTradeDTO {

    private OrderDTO askOrder;
    private OrderDTO bidOrder;

    public MatchedTradeDTO(OrderDTO askOrder, OrderDTO bidOrder){
        this.askOrder = askOrder;
        this.bidOrder = bidOrder;
    }

    public OrderDTO getAskOrder() {
        return askOrder;
    }

    public void setAskOrder(OrderDTO askOrder) {
        this.askOrder = askOrder;
    }

    public OrderDTO getBidOrder() {
        return bidOrder;
    }

    public void setBidOrder(OrderDTO bidOrder) {
        this.bidOrder = bidOrder;
    }
}
