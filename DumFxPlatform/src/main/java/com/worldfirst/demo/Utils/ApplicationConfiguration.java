package com.worldfirst.demo.Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;


@PropertySource(value = "classpath:application.properties")
@Configuration
public class ApplicationConfiguration {

    @Autowired
    private Environment env;

    public String getPrice(String currency) {
        return env.getProperty(currency);
    }

}