package com.worldfirst.demo.facade;

import com.worldfirst.demo.Utils.ApplicationConfiguration;
import com.worldfirst.demo.dto.OrderDTO;
import com.worldfirst.demo.model.MatchedTrade;
import com.worldfirst.demo.dto.MatchedTradeDTO;
import com.worldfirst.demo.model.Order;
import com.worldfirst.demo.model.OrderType;
import com.worldfirst.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class OrderServiceFacade {

    @Autowired
    private OrderService orderService;

    @Autowired
    private ApplicationConfiguration applicationConfiguration;


    public void addLiveOrder(OrderDTO orderDTO){

        Order order = dtoToEntity(orderDTO);
        orderService.addLiveOrder(order);
    }

    public void cancelLiveOrder(OrderDTO orderDTO){
        Order order = dtoToEntity(orderDTO);
        orderService.cancelLiveOrder(order);
    }

    public Map<OrderType,List<Order>> getLiveOrders(){
        return orderService.getLiveOrders();
    }

    public List<MatchedTradeDTO> getMatchedOrders(){
        List<MatchedTrade> matchedTrades =  orderService.getMatchedOrders();
        return matchedTrades.stream().map(matchedTrade ->
                new MatchedTradeDTO(entityToDTO(matchedTrade.getAskOrder()), entityToDTO(matchedTrade.getBidOrder()))).collect(Collectors.toList());
    }

    public List<OrderDTO> getLiveOrdersByType(OrderType orderType){
        List<Order> orders = orderService.getLiveOrdersByType(orderType);
        if(orders != null){
            return orders.stream().map(order -> entityToDTO(order)).collect(Collectors.toList());
        }
        else {
            return new ArrayList<>();
        }
    }

    private Order dtoToEntity(OrderDTO orderDTO){
        double price = Double.parseDouble(applicationConfiguration.getPrice(orderDTO.getCurrency()));
        Order order = new Order(orderDTO.getId(),
                orderDTO.getOrderType(),
                orderDTO.getCurrency(),
                orderDTO.getAmount(), price);

        return order;
    }

    private OrderDTO entityToDTO(Order order){
        OrderDTO orderDTO = new OrderDTO(order.getId(),
                order.getOrderType(),
                order.getCurrency(),
                order.getPrice(),order.getAmount());

        return orderDTO;
    }

}
