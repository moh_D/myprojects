Java Back-End Developer Test
1. Purpose
The goal of this test is to provide us with an understanding of your coding style and skills.
Please pay particular attention to your:
● Choice of data structures
● Quality and use of unit tests
● Consideration of concurrency issues (see non-functional requirements)
● Implementation of the core interface methods
● Code structure
● Design
We totally acknowledge it will be difficult to write a “complete” and top-notch solution in 2 hours, and hence are not expecting this.
Our main goal is to understand your approach to the problem, and assess your knowledge and application of key elements of Java (particularly data structures and concurrency).
We provide you with an interface that you must implement alongside a Gradle build file to help you get started building your project.
2. Description
Write a mini game backend in Java which registers game scores for different users and levels, with the capability to return high score lists per level. There shall also be a simple login system in place (without any authentication...). Deliver a project containing:
● The code (including tests) in the src folder
● An optional readme.txt with thoughts and considerations around the program
3. Functional requirements
The functions are described in detail below.
Users and levels are created “ad-hoc”: the first time they are referenced.
3.1 Login
● This function returns a session key in the form of a string (without spaces or “strange” characters)
● which shall be valid for use with the other functions for 10 minutes.
● The session keys should be “reasonably unique”.
3.2 Post a user's score to a level
● This method can be called several times per user and level and does not return anything.
● Only requests with valid session keys shall be processed.
3.3 Get a high score list for a level
Retrieves the high scores for a specific level.
● The result is a comma separated list in descending score order.
● No more than 15 scores are to be returned for each level.
● Only the highest score counts. ie: an user id can only appear at most once in the list.
● If a user hasn't submitted a score for the level, no score is present for that user.
● A request for a high score list of a level without any scores submitted shall be an empty string.
4. Nonfunctional requirements
● This server will be handling a lot of simultaneous requests, so make good use of the available memory and CPU, while not compromising readability or integrity of the data.
● Do not use any external frameworks, except for testing.
● There is no need for persistence to disk, the application shall be able to run for any foreseeable future without crashing anyway.
5. FAQs
Is there any tech I must use?
Java and JUnit are the only requirements, with a preference for Java 8. We would prefer you implement the code using tech you are familiar with, so we have have a measured discussion afterwards about your design and implementation choices.
You should not use any third party frameworks (except for testing and/lor logging), such as Spring/Hibernate.
