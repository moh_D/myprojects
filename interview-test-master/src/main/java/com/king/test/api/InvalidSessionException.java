package com.king.test.api;

public class InvalidSessionException extends Exception {

	public InvalidSessionException(String message){ super(message); }
}
