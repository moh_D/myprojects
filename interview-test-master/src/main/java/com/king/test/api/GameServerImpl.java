package com.king.test.api;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class GameServerImpl implements GameServer {


	private Map<Integer, Map<String, Integer>> levelScore = new HashMap<>();
	private SecureTokenService secureTokenService;

	public GameServerImpl(SecureTokenService secureTokenService){
		this.secureTokenService = secureTokenService;
	}

	@Override
	public String login(String username) {
		if(username == null || username.equals(""))
			throw new IllegalArgumentException();
		String token = secureTokenService.generateToken();

		return token;
	}

	@Override
	public void submitScore(String sessionKey, UserScore userScore) throws InvalidSessionException {

		if(!secureTokenService.isValid(sessionKey)){
			throw new InvalidSessionException("Session Key is invalied");
		}

		if(userScore == null || !userScore.isValid()){
			throw new IllegalArgumentException();
		}
		Map<String, Integer> level = levelScore.get(userScore.getLevel());

		if (level == null) {
			level = new HashMap<>();
		}
		Integer currentUserscore = level.get(userScore.getUsername());

		if (currentUserscore == null || currentUserscore < userScore.getScore()) {

			level.put(userScore.getUsername(), userScore.getScore());
		}


		levelScore.put(userScore.getLevel(),level);
	}

	@Override
	public String getHighScores(String sessionKey, int level) throws InvalidSessionException {

		if(!secureTokenService.isValid(sessionKey)){
			throw new InvalidSessionException("Session Key is invalied");
		}

		Map<String, Integer> levelData = levelScore.get(level);
		if(levelData == null){
			return "";
		}

		return levelData.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(15).map(entry -> entry.getKey() + " : " + entry.getValue())
				.collect(Collectors.joining(", "));
	}
}
