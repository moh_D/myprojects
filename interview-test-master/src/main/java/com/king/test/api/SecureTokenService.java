package com.king.test.api;

import java.util.UUID;

public class SecureTokenService {

	public static final String TOKEN_SPLIT_REGEX = "\\|";
	public static final int TOKEN_TOTAL_MAX_TTL = 600000;
	public static final String TOKEN_SPLIT = "|";

	public String generateToken(){

		return UUID.randomUUID().toString() + TOKEN_SPLIT + System.currentTimeMillis();
	}


	public boolean isValid(String token){

		if(token == null || token.equals("")){
			return false;
		}

		String[] tokenSplit = token.split(TOKEN_SPLIT_REGEX);

		if(tokenSplit.length != 2){
			return false;
		}
		if(tokenSplit[1] == ""){
			return false;
		}

		Long tokenTime;
		try{
			tokenTime = Long.parseLong(tokenSplit[1]);
		}catch(NumberFormatException e){
			return false;
		}

		return System.currentTimeMillis()-tokenTime < TOKEN_TOTAL_MAX_TTL;
	}
}
