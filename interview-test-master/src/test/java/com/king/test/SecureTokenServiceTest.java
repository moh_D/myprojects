package com.king.test;

import com.king.test.api.SecureTokenService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SecureTokenServiceTest {

	private SecureTokenService secureTokenService;

	@Before
	public void setUp(){
		secureTokenService = new SecureTokenService();
	}

	@Test
	public void generateToken_ValidInput_NonNullResponse(){

		Assert.assertNotNull(secureTokenService.generateToken());
	}

	@Test
	public void generateToken_TokenGenerated_TimeEncodedInToken(){

		String token = secureTokenService.generateToken();
		String[] tokenSplit = token.split(SecureTokenService.TOKEN_SPLIT_REGEX);

		Assert.assertEquals(2, tokenSplit.length);
		Assert.assertTrue(Long.parseLong(tokenSplit[1]) <= System.currentTimeMillis());
	}

	@Test
	public void isValid_NullTokenProvided_ReturnFalse(){

		Assert.assertFalse(secureTokenService.isValid(null));
	}

	@Test
	public void isValid_EmptyTokenProvided_ReturnFalse(){

		Assert.assertFalse(secureTokenService.isValid(""));
	}

	@Test
	public void isValid_MalFormedToken_ReturnFalse(){

		Assert.assertFalse(secureTokenService.isValid("aghdhe-jdjd-pepep"));
	}

	@Test
	public void isValid_MalFormedTokenNoTime_ReturnFalse(){

		Assert.assertFalse(secureTokenService.isValid("aghdhe-jdjd-pepep|"));
	}

	@Test
	public void isValid_MalFormedTokenTimeNotNumber_ReturnFalse(){

		Assert.assertFalse(secureTokenService.isValid("aghdhe-jdjd-pepep|gcgd"));
	}

	@Test
	public void isValid_ValidToken_ReturnTrue(){

		Assert.assertTrue(secureTokenService.isValid("aghdhe-jdjd-pepep-pdidi|"+System.currentTimeMillis()));
	}
}
