package com.king.test;


import com.king.test.api.GameServer;
import com.king.test.api.GameServerImpl;
import com.king.test.api.InvalidSessionException;
import com.king.test.api.SecureTokenService;
import com.king.test.api.UserScore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;



public class GameServerTest {

    private GameServer gameServer;
    private SecureTokenService secureTokenServiceMock;

    @Before
    public void setUp() {
        secureTokenServiceMock = Mockito.mock(SecureTokenService.class);
        gameServer = new GameServerImpl(secureTokenServiceMock);
        Mockito.when(secureTokenServiceMock.generateToken()).thenReturn("default");
    }

    @Test(expected = IllegalArgumentException.class)
    public void login_NullUsername_Exception() {

        Assert.assertNotNull(gameServer.login(null));
    }

    @Test(expected = IllegalArgumentException.class)
    public void login_EmptyUsername_Exception() {

        Assert.assertNotNull(gameServer.login(""));
    }

    @Test
    public void login_ValidInput_NotNullResponse() {

        Assert.assertNotNull(gameServer.login("userName"));
    }

    @Test
    public void login_ValidInput_ExpectedCorrectResult() {

        Mockito.when(secureTokenServiceMock.generateToken()).thenReturn("abc");

        String token = gameServer.login("userName");

        Assert.assertEquals("abc", token);
    }

    @Test(expected = InvalidSessionException.class)
    public void submitScore_NullToken_InvalidSessionException() throws InvalidSessionException{

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        Mockito.when(secureTokenServiceMock.isValid(null)).thenReturn(false);

        gameServer.submitScore(null, new UserScore("username", 1 ,1));
    }

    @Test(expected = InvalidSessionException.class)
    public void submitScore_emptyToken_InvalidSessionException() throws InvalidSessionException{

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        Mockito.when(secureTokenServiceMock.isValid("")).thenReturn(false);

        gameServer.submitScore("", new UserScore("username", 1 ,1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void submitScore_NullUserScore_InvalidArgumentException() throws InvalidSessionException{

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void submitScore_NullUserName_InvalidArgumentException() throws InvalidSessionException{

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", new UserScore(null, 1, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void submitScore_EmptyUserName_InvalidArgumentException() throws InvalidSessionException{

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", new UserScore("", 1, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void submitScore_LevelLowerThanZero_InvalidArgumentException() throws InvalidSessionException{

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", new UserScore("username", -1, 1));
    }

    @Test
    public void submitScore_ValidInput_ExpectedCorrectResult() throws InvalidSessionException {

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", new UserScore("username", 0, 1));
    }

    @Test
    public void submitScore_ScoreSaved_ScoreCanBeRetrieved() throws InvalidSessionException {

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", new UserScore("username", 0, 1));

        String result = gameServer.getHighScores("token", 0);

        Assert.assertEquals("username : 1", result);
    }

    @Test
    public void submitScore_ScoreSavedTwiceForSameUser_HighestScoreCanBeRetrieved() throws InvalidSessionException {

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", new UserScore("username", 0, 1));
        gameServer.submitScore("token", new UserScore("username", 0, 3));

        String result = gameServer.getHighScores("token", 0);

        Assert.assertEquals("username : 3", result);
    }

    @Test
    public void submitScore_ScoreSavedTwiceForSameUser_HighestScoreCanBeRetrievedAfterSubmitLowerScore() throws InvalidSessionException {

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", new UserScore("username", 0, 3));
        gameServer.submitScore("token", new UserScore("username", 0, 1));

        String result = gameServer.getHighScores("token", 0);

        Assert.assertEquals("username : 3", result);
    }

    @Test
    public void submitScore_ScoreSavedForDifferentUser_HighestScoreCanBeRetrieved() throws InvalidSessionException {

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", new UserScore("username", 0, 3));
        gameServer.submitScore("token", new UserScore("username2", 0, 1));

        String result = gameServer.getHighScores("token", 0);

        Assert.assertEquals("username : 3, username2 : 1", result);
    }

    @Test
    public void submitScore_ScoreSavedTwiceForDifferentUser_HighestScoreCanBeRetrievedAfterSubmittingNewScores() throws InvalidSessionException {

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", new UserScore("username", 0, 3));
        gameServer.submitScore("token", new UserScore("username", 0, 1));

        gameServer.submitScore("token", new UserScore("username2", 0, 1));
        gameServer.submitScore("token", new UserScore("username2", 0, 4));


        String result = gameServer.getHighScores("token", 0);

        Assert.assertEquals("username2 : 4, username : 3", result);
    }

    @Test
    public void submitScore_ScoreSavedTwiceForDifferentUserAndDifferentLevel_HighestScoreForLevelZeroRetrievedOnly() throws InvalidSessionException {

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        gameServer.submitScore("token", new UserScore("username", 1, 3));
        gameServer.submitScore("token", new UserScore("username", 0, 1));

        gameServer.submitScore("token", new UserScore("username2", 2, 1));
        gameServer.submitScore("token", new UserScore("username2", 0, 4));


        String result = gameServer.getHighScores("token", 0);

        Assert.assertEquals("username2 : 4, username : 1", result);
    }

    @Test
    public void submitScore_NoScoreIsSubmitted_EmptyStringIsReturned() throws InvalidSessionException {

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        String result = gameServer.getHighScores("token", 0);

        Assert.assertEquals("", result);
    }

    @Test
    public void submitScore_MoreThanTwentyScoresSubmittedInOneLevel_OnlyFifteenReturned() throws InvalidSessionException {

        Mockito.when(secureTokenServiceMock.isValid(Mockito.anyString())).thenReturn(true);

        for (int i = 0; i < 20; i++){
            gameServer.submitScore("token", new UserScore("username"+i, 0, i));
        }

        gameServer.submitScore("token", new UserScore("username0", 0, 21));
        gameServer.submitScore("token", new UserScore("username5", 0, -1));
        gameServer.submitScore("token", new UserScore("username10", 0, 4));

        String result = gameServer.getHighScores("token", 0);

        Assert.assertEquals("username0 : 21, username19 : 19, username18 : 18, username17 : 17, username16 : 16, username15 : 15, username14 : 14, username13 : 13, username12 : 12, username11 : 11, username10 : 10, username9 : 9, username8 : 8, username7 : 7, username6 : 6", result);
    }
}
