package com.ticTacToe.rest.service;

import com.ticTacToe.rest.model.Game;

import org.springframework.stereotype.Service;


/**
 * Created by mohameddababi on 06/09/2017.
 */
@Service
public class GamePlay {

	private final String TIE_RESULT = "Tie";

	/**
	 * Method to play the game
	 * @param game
	 * @param player
	 * @param row
	 * @param col
	 * @return a the new state of the instance
	 * @throws Exception
	 */
	public Game play(Game game, char player, int row, int col) throws Exception {

		allowedPlayer(player);

		setCurrentPlayer(player, game);

		if(game.isOver()) {
			throw new Exception("The game is already over. No more moves can be made.");
		}

		if(String.valueOf(player).equalsIgnoreCase(String.valueOf(Game.PLAYERS.get(game.getCurrentPlayer())))) {
			place(game, row, col);
			game.setMoves(game.getMoves() + 1);

			if (checkWinner(game)) {
				game.setWinner("Player " + game.getCurrentPlayer());
				game.setOver(true);
			} else if (game.getMoves() >= 9) {
				game.setWinner("Tie");
				game.setOver(true);
			} else {
				changePlayer(game);
			}
			if(game.isOver()) {

				throw new Exception("The game is already over. No more moves can be made. " + result(game));
			}
		} else {
			throw new Exception("It is not this player's turn.");
		}

		return game;
	}

	/**
	 * method that places the player's choice on the board and make sure we don't play on the same place again
	 * and check if the current player isn't playing two time in a row
	 * @param game
	 * @param row
	 * @param col
	 * @throws Exception
	 */
	private void place(Game game,int row, int col) throws Exception {
		char[][] tempBoard = game.getBoard();

		if(row > Game.BOARD_SIZE - 1 || row < 0 || col > Game.BOARD_SIZE - 1 || col < 0) {
			throw new Exception(String.format("Space col : %d,row : %d is not on this board", col, row ));
		}

		if(tempBoard[row][col] != '-') {
			throw new Exception(String.format("Space col : %d, row : %d is already occupied", col, row ));
		}
		tempBoard[row][col] = Game.PLAYERS.get(game.getCurrentPlayer());
		game.setBoard(tempBoard);
	}

	private void changePlayer(Game game) {
		int currentPlayer = game.getCurrentPlayer();
		game.setCurrentPlayer((currentPlayer + 1) % 2);
	}

	/**
	 * method that check the winner by checking columns, rows and diagonals
	 * @param game
	 * @return true or false
	 */

	private boolean checkWinner(Game game) {
		int currentPlayer = game.getCurrentPlayer();
		return checkRows(game, Game.PLAYERS.get(currentPlayer)) || checkColumns(game, Game.PLAYERS.get(currentPlayer)) || checkDiagonals(game, Game.PLAYERS.get(currentPlayer));
	}

	private boolean checkRows(Game game, char player) {
		for(char[] row:game.getBoard()) {
			boolean hasWonSoFar = true;
			for(int c = 0; c<row.length && hasWonSoFar; c++) {
				hasWonSoFar &= player == row[c];
			}

			if(hasWonSoFar){
				return true;
			}
		}

		return false;
	}

	private boolean checkColumns(Game game, char player) {
		for(int c = 0; c< game.getBoard()[0].length; c++) {
			boolean hasWonSoFar = true;
			for(int r = 0; r< game.getBoard().length && hasWonSoFar; r++) {
				hasWonSoFar &= player == game.getBoard()[r][c];
			}

			if(hasWonSoFar){
				return true;
			}
		}

		return false;
	}

	private boolean checkDiagonals(Game game, char player) {
		boolean hasWonSoFar;

		hasWonSoFar = true;
		for(int i = 0; i< game.getBoard().length && hasWonSoFar; i++) {
			hasWonSoFar &= player == game.getBoard()[i][i];
		}
		if(hasWonSoFar){
			return true;
		}

		hasWonSoFar = true;
		for(int i = 0; i<game.getBoard().length && hasWonSoFar; i++) {
			hasWonSoFar &= player == game.getBoard()[i][game.getBoard().length - 1 - i];
		}

		return hasWonSoFar;

	}

	/**
	 * method that throws error if the characters players is not X or O
	 * @param player
	 * @throws Exception
	 */
	private void allowedPlayer(char player) throws Exception {
		Boolean isPresent = Game.PLAYERS.contains(player);
		if(!isPresent){
			throw new Exception("This player doesn't exist, choose between X or O ");
		}
	}

	private String result(Game game){
		if(game.getWinner().equals(TIE_RESULT)){
			return "IT'S A TIE";
		}else if(game.getWinner().equals("None")){

			return "";
		}else{

			return "The winner is "+ game.getWinner();
		}
	}

	/**
	 * Method used in the beginning of the game to set the first player as X or O
	 * @param player
	 * @param game
	 */
	private void setCurrentPlayer(char player, Game game){
		if(game.getMoves()==0){
			int position = Game.PLAYERS.indexOf(player);
			game.setCurrentPlayer(position);
		}
	}
}
