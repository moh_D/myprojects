package com.ticTacToe.rest.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by mohameddababi on 05/09/2017.
 */

public class Game {
	public final static int BOARD_SIZE = 3;
	public final static List<Character> PLAYERS = Arrays.asList('X', 'O');;

	private char[][] board;
	private int currentPlayer;
	private String winner;
	private boolean isOver;
	private int moves;


	public String getTurn() {
		return "Player " + PLAYERS.get(currentPlayer);
	}


	public char[][] getBoard() {
		return board;
	}

	public Game() {
		board = new char[BOARD_SIZE][BOARD_SIZE];
		boardInit();
		isOver = false;
		moves = 0;
		winner = "None";
		currentPlayer = 0;
	}


	private void boardInit() {
		for (char[] row : board) {
			Arrays.fill(row, '-');
		}
	}

	public void boardDisplay(){
		System.out.println("------------");
		for(int i = 0; i < BOARD_SIZE; i++){
			System.out.print("| ");
			for(int j= 0; j < BOARD_SIZE; j++){
				System.out.print(board[i][j] + " | " );
				if(j == BOARD_SIZE -1){
					System.out.println();
				}
			}
			System.out.println("------------");
		}
	}


	public void setBoard(char[][] board) {
		this.board = board;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	public boolean isOver() {
		return isOver;
	}

	public void setOver(boolean over) {
		isOver = over;
	}

	public int getMoves() {
		return moves;
	}

	public void setMoves(int moves) {
		this.moves = moves;
	}

	public String getWinner() {
		return winner;
	}

	@Override
	public String toString() {
		return "Game{" +
				" board= " + boardToString() +
				", currentPlayer= " + currentPlayer +
				", winner= '" + winner + '\'' +
				", isOver= " + isOver +
				", moves= " + moves +
				", turn : "+ getTurn() +
				'}';
	}

	private String boardToString(){
		return Arrays
				.stream(board)
				.map(Arrays::toString)
				.collect(Collectors.joining(System.lineSeparator()));
	}
}
