package com;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ticTacToe.rest.model.Game;
import com.ticTacToe.rest.service.GamePlay;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;



@Controller
@Validated
@SessionAttributes("game")
public class GamesController {

	@Autowired
	GamePlay gamePlay;

	/**
	 * Create a new game in session
	 * @param modelMap
	 * @return
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/toNewGame", method = RequestMethod.GET)
	public String createNewGame(ModelMap modelMap) throws JsonProcessingException {

		Game game = new Game();

		modelMap.addAttribute("game", game);



		modelMap.addAttribute("gameJson", game.toString());
		game.boardDisplay();
		return "gamePage";
	}

	/**
	 * to redirect from error page
	 * @param game
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/toGame", method = RequestMethod.GET)
	public String redirect(@ModelAttribute(value="game") Game game, ModelMap modelMap) {

		modelMap.addAttribute("game", game);

		modelMap.addAttribute("gameJson", game.toString());
		game.boardDisplay();
		return "gamePage";
	}

	/**
	 * to play the game
	 * @param row
	 * @param col
	 * @param player
	 * @param game
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/playGame", method = RequestMethod.GET)
	public String updateGame(@RequestParam int row,
	                         @RequestParam int col,
	                         @RequestParam char player,
	                         @ModelAttribute(value="game") Game game, ModelMap modelMap) {
		try {
			game = gamePlay.play(game, player, row, col);
			game.boardDisplay();

			System.out.println("It is "+ game.getTurn() + " turn");

			modelMap.addAttribute("gameJson", game.toString());

		} catch (Exception e) {
			//for Front end
			modelMap.addAttribute("errorMessage", e.getMessage());

			modelMap.addAttribute("gameJson", game.toString());
			System.out.println(e.getMessage());
			if(game.isOver()){
				game.boardDisplay();
			}
		}


		return "gamePage" ;
	}

	/**
	 * create a new instance of game and add it to session when /playGame is called directly before /toNewGame
	 * @return
	 */
	@ModelAttribute("game")
	public Game getGame() {
		return new Game();

	}

	@ModelAttribute("gameJson")
	public String getGameJson() {
		return "";

	}

}
