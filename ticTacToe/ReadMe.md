This is the Tic tac toe Game
============================

GamesController :
=================


### /toNewGame : 
Create a new instance of the game and put it in session

### /playGame : 
Call the method play in GamePlay and modify the state of the game 

### /toGame : 
Redirect the gamePage when an error is thrown 


I use the session to save the state of the game while the game goes on. so different players can play at the sametime


Service / GamePlay :
===================

This Class is the heart of the game, all the logic is found in it.

All the methods in that class have a description on top of them. Feel free to take a look.


How to Play :
============


- You will start the game by going to the page game with this link : http://localhost:8080/toNewGame
you will see an empty board 

- To Start playing and putting X or O on the board you will use this link : http://localhost:8080/playGame?col=col&row=row&player=player 
   - col = choice between 0 to 2
   - row = choice between 0 to 2
   - player = choice between X or Y 

The page will reload with the new state of the game.
