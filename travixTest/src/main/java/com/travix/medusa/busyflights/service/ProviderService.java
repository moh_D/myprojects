package com.travix.medusa.busyflights.service;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProviderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProviderService.class);

    /**
     * return a list of both flights that were found
     * @param busyFlightsRequest
     * @return
     * @throws Exception
     */
    public List<BusyFlightsResponse> findBusyFlights(BusyFlightsRequest busyFlightsRequest) throws Exception {
        BusyFlightsResponse busyFlightsResponseFromToughtJet = findToughJet(busyFlightsRequest);
        BusyFlightsResponse busyFlightsResponseFromCrazyAirFlight = findCrazyAirFlights(busyFlightsRequest);

        List<BusyFlightsResponse> busyFlightsResponses = new ArrayList<>();
        busyFlightsResponses.add(busyFlightsResponseFromToughtJet);
        busyFlightsResponses.add(busyFlightsResponseFromCrazyAirFlight);

        return busyFlightsResponses;
    }

    /**
     * Make the call to find the CrazyAir flight and transform to return a busyFlightResponse
     * @param busyFlightsRequest
     * @return
     * @throws Exception
     */
    public BusyFlightsResponse findCrazyAirFlights(BusyFlightsRequest busyFlightsRequest) throws Exception{

        CrazyAirRequest crazyAirRequest = CrazyAirRequest.transformToCrazyAirRequest(busyFlightsRequest);

        //we make the call and get the response


        LOGGER.info("Getting flights from Crazy Air");

        CrazyAirResponse crazyAirResponse ;
        Client client = ClientBuilder.newClient();
        Entity payload = Entity.entity(crazyAirRequest, MediaType.APPLICATION_JSON_TYPE);

        Response response = client.target("https://crazyair/api/getFlights")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(payload);

        if (!(response.getStatus() == 201)) {
            LOGGER.error("Response status : " + response.getStatus());
            LOGGER.error("Response header : " + response.getHeaders());
            LOGGER.error("Reponse body : " + response.readEntity(String.class));

            throw new Exception("Something happened while retrieving the flight result");
        }else {
            crazyAirResponse = response.readEntity(CrazyAirResponse.class);
        }


        return crazyAirResponse.transformToBusyFlightsResponse();
    }

    /**
     * Make the call to find the ToughtJet flight and transform to return a busyFlightResponse
     * @param busyFlightsRequest
     * @return
     * @throws Exception
     */
    public BusyFlightsResponse findToughJet(BusyFlightsRequest busyFlightsRequest) throws Exception{

        ToughJetRequest toughJetRequest = ToughJetRequest.transformToCrazyAirRequest(busyFlightsRequest);

        //we make the call and get the response
        //It doesn't work as we don't have the right url
        ToughJetResponse toughJetResponse;
        Client client = ClientBuilder.newClient();
        Entity payload = Entity.entity(toughJetRequest, MediaType.APPLICATION_JSON_TYPE);

        Response response = client.target("https://C/api/getFlights")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(payload);

        if (!(response.getStatus() == 201)) {
            LOGGER.error("Response status : " + response.getStatus());
            LOGGER.error("Response header : " + response.getHeaders());
            LOGGER.error("Reponse body : " + response.readEntity(String.class));

            throw new Exception("Something happened while retrieving the flight result");
        }else {
            toughJetResponse = response.readEntity(ToughJetResponse.class);
        }

        //then we transform it to a busyFlightResponse
        return toughJetResponse.transformToBusyFlightsResponse() ;
    }

}
