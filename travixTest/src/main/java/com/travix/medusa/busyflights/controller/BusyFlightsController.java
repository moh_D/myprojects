package com.travix.medusa.busyflights.controller;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;
import com.travix.medusa.busyflights.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BusyFlightsController {

    @Autowired
    private ProviderService providerService;

    @RequestMapping(value = "/getFlights", method = RequestMethod.POST)
    public @ResponseBody List<BusyFlightsResponse> findBusyFlights(@RequestBody BusyFlightsRequest busyFlightsRequest) throws Exception {
        List<BusyFlightsResponse> busyFlightsResponses = providerService.findBusyFlights(busyFlightsRequest);
        
        return busyFlightsResponses;
    }


}
