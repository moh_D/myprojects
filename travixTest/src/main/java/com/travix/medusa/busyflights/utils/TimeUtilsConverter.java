package com.travix.medusa.busyflights.utils;

import java.time.*;

public class TimeUtilsConverter {

    public static ZonedDateTime toZonedDateTime (LocalDateTime localDateTime){

        ZonedDateTime zonedDateTime = ZonedDateTime.from(localDateTime);

        return zonedDateTime;
    }

    public static ZonedDateTime toZonedDateTime (Instant instant){

        ZonedDateTime zonedDateTime = instant.atZone(ZoneId.of(ZoneOffset.UTC.getId()));

        return zonedDateTime;
    }
}
