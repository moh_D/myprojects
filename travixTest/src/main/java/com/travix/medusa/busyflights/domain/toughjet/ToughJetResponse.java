package com.travix.medusa.busyflights.domain.toughjet;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;
import com.travix.medusa.busyflights.utils.InstantDeserializer;
import com.travix.medusa.busyflights.utils.LocalDateTimeDeserializer;
import com.travix.medusa.busyflights.utils.TimeUtilsConverter;

import java.time.Instant;

public class ToughJetResponse {

    private String carrier;
    private double basePrice;
    private double tax;
    private double discount;
    private String departureAirportName;
    private String arrivalAirportName;

    @JsonDeserialize(using = InstantDeserializer.class)
    private Instant outboundDateTime;

    @JsonDeserialize(using = InstantDeserializer.class)
    private Instant inboundDateTime;

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(final String carrier) {
        this.carrier = carrier;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(final double basePrice) {
        this.basePrice = basePrice;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(final double tax) {
        this.tax = tax;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(final double discount) {
        this.discount = discount;
    }

    public String getDepartureAirportName() {
        return departureAirportName;
    }

    public void setDepartureAirportName(final String departureAirportName) {
        this.departureAirportName = departureAirportName;
    }

    public String getArrivalAirportName() {
        return arrivalAirportName;
    }

    public void setArrivalAirportName(final String arrivalAirportName) {
        this.arrivalAirportName = arrivalAirportName;
    }

    public Instant getOutboundDateTime() {
        return outboundDateTime;
    }

    public void setOutboundDateTime(final Instant outboundDateTime) {
        this.outboundDateTime = outboundDateTime;
    }

    public Instant getInboundDateTime() {
        return inboundDateTime;
    }

    public void setInboundDateTime(final Instant inboundDateTime) {
        this.inboundDateTime = inboundDateTime;
    }

    public BusyFlightsResponse transformToBusyFlightsResponse(){
        BusyFlightsResponse busyFlightsResponse = new BusyFlightsResponse();
        busyFlightsResponse.setAirline(carrier);
        busyFlightsResponse.setFare(finalFare());
        busyFlightsResponse.setDepartureAirportCode(departureAirportName);
        busyFlightsResponse.setDestinationAirportCode(arrivalAirportName);
        busyFlightsResponse.setDepartureDate(TimeUtilsConverter.toZonedDateTime(outboundDateTime));
        busyFlightsResponse.setArrivalDate(TimeUtilsConverter.toZonedDateTime(inboundDateTime));
        busyFlightsResponse.setSupplier("ToughJet");

        return busyFlightsResponse;
    }

    private Double finalFare(){
        return ((100-discount)*(basePrice+tax))/100;
    }
}
