package com.travix.medusa.busyflights.domain.crazyair;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;
import com.travix.medusa.busyflights.utils.LocalDateTimeDeserializer;
import com.travix.medusa.busyflights.utils.TimeUtilsConverter;

import java.io.Serializable;
import java.time.LocalDateTime;

public class CrazyAirResponse implements Serializable{

    @JsonIgnore
    private static final String CRAZY_AIR = "Crazy Air";

    private String airline;
    private double price;
    private String cabinclass;
    private String departureAirportCode;
    private String destinationAirportCode;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime departureDate;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime arrivalDate;

    public String getAirline() {
        return airline;
    }

    public void setAirline(final String airline) {
        this.airline = airline;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

    public String getCabinclass() {
        return cabinclass;
    }

    public void setCabinclass(final String cabinclass) {
        this.cabinclass = cabinclass;
    }

    public String getDepartureAirportCode() {
        return departureAirportCode;
    }

    public void setDepartureAirportCode(final String departureAirportCode) {
        this.departureAirportCode = departureAirportCode;
    }

    public String getDestinationAirportCode() {
        return destinationAirportCode;
    }

    public void setDestinationAirportCode(final String destinationAirportCode) {
        this.destinationAirportCode = destinationAirportCode;
    }

    public LocalDateTime getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(final LocalDateTime departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDateTime getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(final LocalDateTime arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public BusyFlightsResponse transformToBusyFlightsResponse(){
        BusyFlightsResponse busyFlightsResponse = new BusyFlightsResponse();
        busyFlightsResponse.setAirline(airline);
        busyFlightsResponse.setFare(price);
        busyFlightsResponse.setDepartureAirportCode(departureAirportCode);
        busyFlightsResponse.setDestinationAirportCode(destinationAirportCode);
        busyFlightsResponse.setDepartureDate(TimeUtilsConverter.toZonedDateTime(departureDate));
        busyFlightsResponse.setArrivalDate(TimeUtilsConverter.toZonedDateTime(arrivalDate));
        busyFlightsResponse.setSupplier(CRAZY_AIR);

        return busyFlightsResponse;
    }
}
