package com.travix.medusa.busyflights.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.Instant;

public class InstantDeserializer extends StdDeserializer<Instant> {

    private static final long serialVersionUID = 1L;

    protected InstantDeserializer() {
        super(Instant.class);
    }


    @Override
    public Instant deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
        return Instant.parse(jp.readValueAs(String.class));
    }
}
