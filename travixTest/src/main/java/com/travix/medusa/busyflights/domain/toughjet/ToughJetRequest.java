package com.travix.medusa.busyflights.domain.toughjet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.utils.LocalDateSerializer;

import java.io.Serializable;
import java.time.LocalDate;

public class ToughJetRequest implements Serializable{

    @JsonIgnore
    private static final String CRAZY_AIR = "Crazy Air";

    private String from;
    private String to;

    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate outboundDate;

    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate inboundDate;
    private int numberOfAdults;

    public String getFrom() {
        return from;
    }

    public void setFrom(final String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(final String to) {
        this.to = to;
    }

    public LocalDate getOutboundDate() {
        return outboundDate;
    }

    public void setOutboundDate(final LocalDate outboundDate) {
        this.outboundDate = outboundDate;
    }

    public LocalDate getInboundDate() {
        return inboundDate;
    }

    public void setInboundDate(final LocalDate inboundDate) {
        this.inboundDate = inboundDate;
    }

    public int getNumberOfAdults() {
        return numberOfAdults;
    }

    public void setNumberOfAdults(final int numberOfAdults) {
        this.numberOfAdults = numberOfAdults;
    }

    public static ToughJetRequest transformToCrazyAirRequest(BusyFlightsRequest busyFlightsRequest){

        ToughJetRequest toughJetRequest = new ToughJetRequest();

        toughJetRequest.setFrom(busyFlightsRequest.getOrigin());
        toughJetRequest.setTo(busyFlightsRequest.getDestination());
        toughJetRequest.setInboundDate(busyFlightsRequest.getDepartureDate());
        toughJetRequest.setOutboundDate(busyFlightsRequest.getReturnDate());
        toughJetRequest.setNumberOfAdults(busyFlightsRequest.getNumberOfPassengers());

        return toughJetRequest;
    }
}
