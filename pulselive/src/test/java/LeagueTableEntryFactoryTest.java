import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class LeagueTableEntryFactoryTest {


	@Test
	public void createLeagueTableEntryTest(){

		Match match  = new Match("france","england", 2, 0);

		LeagueTableEntry leagueTableEntry = LeagueTableEntryFactory.createLeagueTableEntry(match, "france");

		Assert.assertEquals("france", leagueTableEntry.getTeamName());
		Assert.assertEquals(0, leagueTableEntry.getDrawn());
		Assert.assertEquals(1, leagueTableEntry.getWon());
		Assert.assertEquals(0, leagueTableEntry.getLost());
		Assert.assertEquals(3, leagueTableEntry.getPoints());
		Assert.assertEquals(2, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(2, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(0, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(1, leagueTableEntry.getPlayed());

		LeagueTableEntry leagueTableEntry1 = LeagueTableEntryFactory.createLeagueTableEntry(match, "england");

		Assert.assertEquals("england", leagueTableEntry1.getTeamName());
		Assert.assertEquals(0, leagueTableEntry1.getDrawn());
		Assert.assertEquals(0, leagueTableEntry1.getWon());
		Assert.assertEquals(1, leagueTableEntry1.getLost());
		Assert.assertEquals(0, leagueTableEntry1.getPoints());
		Assert.assertEquals(-2, leagueTableEntry1.getGoalDifference());
		Assert.assertEquals(0, leagueTableEntry1.getGoalsFor());
		Assert.assertEquals(2, leagueTableEntry1.getGoalsAgainst());
		Assert.assertEquals(1, leagueTableEntry1.getPlayed());

	}

	@Test
	public void createLeagueTableEntryTest1(){

		Match match  = new Match("france","england", 2, 2);

		LeagueTableEntry leagueTableEntry = LeagueTableEntryFactory.createLeagueTableEntry(match, "france");

		Assert.assertEquals("france", leagueTableEntry.getTeamName());
		Assert.assertEquals(1, leagueTableEntry.getDrawn());
		Assert.assertEquals(0, leagueTableEntry.getWon());
		Assert.assertEquals(0, leagueTableEntry.getLost());
		Assert.assertEquals(1, leagueTableEntry.getPoints());
		Assert.assertEquals(0, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(2, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(2, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(1, leagueTableEntry.getPlayed());

		LeagueTableEntry leagueTableEntry1 = LeagueTableEntryFactory.createLeagueTableEntry(match, "england");

		Assert.assertEquals("england", leagueTableEntry1.getTeamName());
		Assert.assertEquals(1, leagueTableEntry1.getDrawn());
		Assert.assertEquals(0, leagueTableEntry1.getWon());
		Assert.assertEquals(0, leagueTableEntry1.getLost());
		Assert.assertEquals(1, leagueTableEntry1.getPoints());
		Assert.assertEquals(0, leagueTableEntry1.getGoalDifference());
		Assert.assertEquals(2, leagueTableEntry1.getGoalsFor());
		Assert.assertEquals(2, leagueTableEntry1.getGoalsAgainst());
		Assert.assertEquals(1, leagueTableEntry1.getPlayed());

	}

	@Test
	public void createLeagueTableEntryTest2(){

		Match match  = new Match("france","england", 1, 2);

		LeagueTableEntry leagueTableEntry = LeagueTableEntryFactory.createLeagueTableEntry(match, "france");

		Assert.assertEquals("france", leagueTableEntry.getTeamName());
		Assert.assertEquals(0, leagueTableEntry.getDrawn());
		Assert.assertEquals(0, leagueTableEntry.getWon());
		Assert.assertEquals(1, leagueTableEntry.getLost());
		Assert.assertEquals(0, leagueTableEntry.getPoints());
		Assert.assertEquals(-1, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(1, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(2, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(1, leagueTableEntry.getPlayed());

		LeagueTableEntry leagueTableEntry1 = LeagueTableEntryFactory.createLeagueTableEntry(match, "england");

		Assert.assertEquals("england", leagueTableEntry1.getTeamName());
		Assert.assertEquals(0, leagueTableEntry1.getDrawn());
		Assert.assertEquals(1, leagueTableEntry1.getWon());
		Assert.assertEquals(0, leagueTableEntry1.getLost());
		Assert.assertEquals(3, leagueTableEntry1.getPoints());
		Assert.assertEquals(1, leagueTableEntry1.getGoalDifference());
		Assert.assertEquals(2, leagueTableEntry1.getGoalsFor());
		Assert.assertEquals(1, leagueTableEntry1.getGoalsAgainst());
		Assert.assertEquals(1, leagueTableEntry1.getPlayed());

	}
}
