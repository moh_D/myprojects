import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LeagueTableTest {

	private LeagueTable leagueTable;
	private Map<String, LeagueTableEntry> leagueTableEntryMap;

	@Before
	public void setUp(){
		Match match  = new Match("france","england", 2, 0);
		Match match1 = new Match("france","swiss", 2, 2);
		Match match2 = new Match("france","danemark", 2, 1);

		Match match3 = new Match("england","swiss", 2, 0);
		Match match4 = new Match("england","danemark", 2, 2);

		Match match5 = new Match("danemark","swiss", 2, 2);

		List<Match> matches = new ArrayList<>();
		matches.add(match);
		matches.add(match1);
		matches.add(match2);
		matches.add(match3);
		matches.add(match4);
		matches.add(match5);
		leagueTable = new LeagueTable(matches);
		leagueTableEntryMap = leagueTable.getMapEntries();
	}

	@Test
	public void getMapEntriesTest() {

		LeagueTableEntry leagueTableEntry = leagueTableEntryMap.get("france");

		Assert.assertEquals("france", leagueTableEntry.getTeamName());
		Assert.assertEquals(3, leagueTableEntry.getPlayed());
		Assert.assertEquals(2, leagueTableEntry.getWon());
		Assert.assertEquals(0, leagueTableEntry.getLost());
		Assert.assertEquals(1, leagueTableEntry.getDrawn());
		Assert.assertEquals(6, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(3, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(3, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(7, leagueTableEntry.getPoints());

	}

	@Test
	public void getMapEntriesTest1() {

		LeagueTableEntry leagueTableEntry = leagueTableEntryMap.get("england");

		Assert.assertEquals("england", leagueTableEntry.getTeamName());
		Assert.assertEquals(3, leagueTableEntry.getPlayed());
		Assert.assertEquals(1, leagueTableEntry.getWon());
		Assert.assertEquals(1, leagueTableEntry.getLost());
		Assert.assertEquals(1, leagueTableEntry.getDrawn());
		Assert.assertEquals(4, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(4, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(0, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(4, leagueTableEntry.getPoints());
	}

	@Test
	public void getMapEntriesTest2() {

		LeagueTableEntry leagueTableEntry = leagueTableEntryMap.get("swiss");

		Assert.assertEquals("swiss", leagueTableEntry.getTeamName());
		Assert.assertEquals(3, leagueTableEntry.getPlayed());
		Assert.assertEquals(0, leagueTableEntry.getWon());
		Assert.assertEquals(1, leagueTableEntry.getLost());
		Assert.assertEquals(2, leagueTableEntry.getDrawn());
		Assert.assertEquals(4, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(6, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(-2, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(2, leagueTableEntry.getPoints());
	}

	@Test
	public void getMapEntriesTest3() {

		LeagueTableEntry leagueTableEntry = leagueTableEntryMap.get("danemark");

		Assert.assertEquals("danemark", leagueTableEntry.getTeamName());
		Assert.assertEquals(3, leagueTableEntry.getPlayed());
		Assert.assertEquals(0, leagueTableEntry.getWon());
		Assert.assertEquals(1, leagueTableEntry.getLost());
		Assert.assertEquals(2, leagueTableEntry.getDrawn());
		Assert.assertEquals(5, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(6, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(-1, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(2, leagueTableEntry.getPoints());
	}

	@Test
	public void getMapEntriesTest4() {

		List<Match> matches = null;
		leagueTable = new LeagueTable(matches);
		leagueTableEntryMap = leagueTable.getMapEntries();

		LeagueTableEntry leagueTableEntry = leagueTableEntryMap.get("danemark");

		Assert.assertNotNull(leagueTableEntryMap);

		Assert.assertNull(leagueTableEntry);

	}

	@Test
	public void getTableEntriesTest() {

		List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntries();

		Assert.assertNotNull(leagueTableEntries);
		Assert.assertEquals(4, leagueTableEntries.size());

		LeagueTableEntry leagueTableEntry0 = leagueTableEntries.get(0);
		Assert.assertEquals("france", leagueTableEntry0.getTeamName());

		LeagueTableEntry leagueTableEntry1 = leagueTableEntries.get(1);
		Assert.assertEquals("england", leagueTableEntry1.getTeamName());

		LeagueTableEntry leagueTableEntry2 = leagueTableEntries.get(2);
		Assert.assertEquals("danemark", leagueTableEntry2.getTeamName());

		LeagueTableEntry leagueTableEntry3 = leagueTableEntries.get(3);
		Assert.assertEquals("swiss", leagueTableEntry3.getTeamName());

	}

	@Test
	public void getTableEntriesTest1() {

		List<Match> matches = null;
		leagueTable = new LeagueTable(matches);
		List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntries();

		Assert.assertNotNull(leagueTableEntries);
		Assert.assertEquals(0, leagueTableEntries.size());

	}



}
