import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LeagueTableEntryTest {

	private LeagueTable leagueTable;
	private Map<String, LeagueTableEntry> leagueTableEntryMap;


	@Before
	public void setUp(){
		Match match  = new Match("france","england", 2, 0);
		Match match1 = new Match("france","swiss", 2, 2);
		Match match2 = new Match("france","danemark", 2, 1);


		List<Match> matches = new ArrayList<>();
		matches.add(match);
		matches.add(match1);
		matches.add(match2);

		leagueTable = new LeagueTable(matches);
		leagueTableEntryMap = leagueTable.getMapEntries();
	}

	@Test
	public void modifyLeagueTableEntryTest(){

		Match match = new Match("england","swiss", 2, 0);

		LeagueTableEntry leagueTableEntry = leagueTableEntryMap.get("england");

		Assert.assertEquals("england", leagueTableEntry.getTeamName());
		Assert.assertEquals(1, leagueTableEntry.getPlayed());
		Assert.assertEquals(0, leagueTableEntry.getWon());
		Assert.assertEquals(1, leagueTableEntry.getLost());
		Assert.assertEquals(0, leagueTableEntry.getDrawn());
		Assert.assertEquals(0, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(2, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(-2, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(0, leagueTableEntry.getPoints());

		leagueTableEntry.modifyLeagueTableEntry(match,"england");

		Assert.assertEquals("england", leagueTableEntry.getTeamName());
		Assert.assertEquals(2, leagueTableEntry.getPlayed());
		Assert.assertEquals(1, leagueTableEntry.getWon());
		Assert.assertEquals(1, leagueTableEntry.getLost());
		Assert.assertEquals(0, leagueTableEntry.getDrawn());
		Assert.assertEquals(2, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(2, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(0, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(3, leagueTableEntry.getPoints());

		// Away team test

		LeagueTableEntry leagueTableEntrySwiss = leagueTableEntryMap.get("swiss");

		Assert.assertEquals("swiss", leagueTableEntrySwiss.getTeamName());
		Assert.assertEquals(1, leagueTableEntrySwiss.getPlayed());
		Assert.assertEquals(0, leagueTableEntrySwiss.getWon());
		Assert.assertEquals(0, leagueTableEntrySwiss.getLost());
		Assert.assertEquals(1, leagueTableEntrySwiss.getDrawn());
		Assert.assertEquals(2, leagueTableEntrySwiss.getGoalsFor());
		Assert.assertEquals(2, leagueTableEntrySwiss.getGoalsAgainst());
		Assert.assertEquals(0, leagueTableEntrySwiss.getGoalDifference());
		Assert.assertEquals(1, leagueTableEntrySwiss.getPoints());

		leagueTableEntrySwiss.modifyLeagueTableEntry(match,"swiss");

		Assert.assertEquals("swiss", leagueTableEntrySwiss.getTeamName());
		Assert.assertEquals(2, leagueTableEntrySwiss.getPlayed());
		Assert.assertEquals(0, leagueTableEntrySwiss.getWon());
		Assert.assertEquals(1, leagueTableEntrySwiss.getLost());
		Assert.assertEquals(1, leagueTableEntrySwiss.getDrawn());
		Assert.assertEquals(2, leagueTableEntrySwiss.getGoalsFor());
		Assert.assertEquals(4, leagueTableEntrySwiss.getGoalsAgainst());
		Assert.assertEquals(-2, leagueTableEntrySwiss.getGoalDifference());
		Assert.assertEquals(1, leagueTableEntrySwiss.getPoints());
	}

	@Test
	public void modifyLeagueTableEntryTest1(){

		Match match = new Match("england","danemark", 2, 2);;

		LeagueTableEntry leagueTableEntry = leagueTableEntryMap.get("england");

		Assert.assertEquals("england", leagueTableEntry.getTeamName());
		Assert.assertEquals(1, leagueTableEntry.getPlayed());
		Assert.assertEquals(0, leagueTableEntry.getWon());
		Assert.assertEquals(1, leagueTableEntry.getLost());
		Assert.assertEquals(0, leagueTableEntry.getDrawn());
		Assert.assertEquals(0, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(2, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(-2, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(0, leagueTableEntry.getPoints());

		leagueTableEntry.modifyLeagueTableEntry(match,"england");

		Assert.assertEquals("england", leagueTableEntry.getTeamName());
		Assert.assertEquals(2, leagueTableEntry.getPlayed());
		Assert.assertEquals(0, leagueTableEntry.getWon());
		Assert.assertEquals(1, leagueTableEntry.getLost());
		Assert.assertEquals(1, leagueTableEntry.getDrawn());
		Assert.assertEquals(2, leagueTableEntry.getGoalsFor());
		Assert.assertEquals(4, leagueTableEntry.getGoalsAgainst());
		Assert.assertEquals(-2, leagueTableEntry.getGoalDifference());
		Assert.assertEquals(1, leagueTableEntry.getPoints());

		LeagueTableEntry leagueTableEntrySwiss = leagueTableEntryMap.get("danemark");

		Assert.assertEquals("danemark", leagueTableEntrySwiss.getTeamName());
		Assert.assertEquals(1, leagueTableEntrySwiss.getPlayed());
		Assert.assertEquals(0, leagueTableEntrySwiss.getWon());
		Assert.assertEquals(1, leagueTableEntrySwiss.getLost());
		Assert.assertEquals(0, leagueTableEntrySwiss.getDrawn());
		Assert.assertEquals(1, leagueTableEntrySwiss.getGoalsFor());
		Assert.assertEquals(2, leagueTableEntrySwiss.getGoalsAgainst());
		Assert.assertEquals(-1, leagueTableEntrySwiss.getGoalDifference());
		Assert.assertEquals(0, leagueTableEntrySwiss.getPoints());

		leagueTableEntrySwiss.modifyLeagueTableEntry(match,"danemark");

		Assert.assertEquals("danemark", leagueTableEntrySwiss.getTeamName());
		Assert.assertEquals(2, leagueTableEntrySwiss.getPlayed());
		Assert.assertEquals(0, leagueTableEntrySwiss.getWon());
		Assert.assertEquals(1, leagueTableEntrySwiss.getLost());
		Assert.assertEquals(1, leagueTableEntrySwiss.getDrawn());
		Assert.assertEquals(3, leagueTableEntrySwiss.getGoalsFor());
		Assert.assertEquals(4, leagueTableEntrySwiss.getGoalsAgainst());
		Assert.assertEquals(-1, leagueTableEntrySwiss.getGoalDifference());
		Assert.assertEquals(1, leagueTableEntrySwiss.getPoints());
	}
}
