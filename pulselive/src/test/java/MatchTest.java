import org.junit.Assert;
import org.junit.Test;

public class MatchTest {

	@Test
	public void isDrawTest() {

		Match match  = new Match("france","england", 2, 0);

		Assert.assertEquals(match.isDraw(), false);
	}

	@Test
	public void isDrawTest1() {

		Match match  = new Match("france","england", 2, 2);

		Assert.assertEquals(match.isDraw(), true);
	}

	@Test
	public void isDrawTest2() {

		Match match  = new Match("france","england", 1, 2);

		Assert.assertEquals(match.isDraw(), false);
	}


	@Test
	public void hasLostTest() {

		Match match  = new Match("france","england", 2, 0);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.hasLost(homeTeam), false);
		Assert.assertEquals(match.hasLost(awayTeam), true);
	}

	@Test
	public void hasLostTest1() {

		Match match  = new Match("france","england", 1, 2);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.hasLost(homeTeam), true);
		Assert.assertEquals(match.hasLost(awayTeam), false);
	}

	@Test
	public void hasLostTest3() {

		Match match  = new Match("france","england", 2, 2);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.hasLost(homeTeam), false);
		Assert.assertEquals(match.hasLost(awayTeam), false);
	}

	@Test
	public void hasWonTest() {

		Match match  = new Match("france","england", 2, 0);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.hasWon(homeTeam), true);
		Assert.assertEquals(match.hasWon(awayTeam), false);
	}

	@Test
	public void hasWonTest1() {

		Match match  = new Match("france","england", 1, 2);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.hasWon(homeTeam), false);
		Assert.assertEquals(match.hasWon(awayTeam), true);
	}

	@Test
	public void hasWonTest3() {

		Match match  = new Match("france","england", 2, 2);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.hasWon(homeTeam), false);
		Assert.assertEquals(match.hasWon(awayTeam), false);
	}

	@Test
	public void getPointTest() {

		Match match  = new Match("france","england", 2, 0);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.getPoint(homeTeam), 3);
		Assert.assertEquals(match.getPoint(awayTeam), 0);
	}

	@Test
	public void getPointTest1() {

		Match match  = new Match("france","england", 1, 2);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.getPoint(homeTeam), 0);
		Assert.assertEquals(match.getPoint(awayTeam), 3);
	}

	@Test
	public void getPointTest2() {

		Match match  = new Match("france","england", 2, 2);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.getPoint(homeTeam), 1);
		Assert.assertEquals(match.getPoint(awayTeam), 1);
	}

	@Test
	public void getHomeTeamGoalsTest() {

		Match match  = new Match("france","england", 2, 0);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.getHomeTeamGoals(homeTeam), 2);
		Assert.assertEquals(match.getHomeTeamGoals(awayTeam), 0);
	}

	@Test
	public void getHomeTeamGoalsTest1() {

		Match match  = new Match("france","england", 1, 2);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.getHomeTeamGoals(homeTeam), 1);
		Assert.assertEquals(match.getHomeTeamGoals(awayTeam), 2);
	}

	@Test
	public void getHomeTeamGoalsTest2() {

		Match match  = new Match("france","england", 2, 2);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.getHomeTeamGoals(homeTeam), 2);
		Assert.assertEquals(match.getHomeTeamGoals(awayTeam), 2);
	}

	@Test
	public void getAwayTeamGoalsTest() {

		Match match  = new Match("france","england", 2, 0);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.getAwayTeamGoals(homeTeam), 0);
		Assert.assertEquals(match.getAwayTeamGoals(awayTeam), 2);
	}

	@Test
	public void getAwayTeamGoalsTest1() {

		Match match  = new Match("france","england", 1, 2);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.getAwayTeamGoals(homeTeam), 2);
		Assert.assertEquals(match.getAwayTeamGoals(awayTeam), 1);
	}

	@Test
	public void getAwayTeamGoalsTest2() {

		Match match  = new Match("france","england", 2, 2);
		String homeTeam = match.getHomeTeam();
		String awayTeam = match.getAwayTeam();

		Assert.assertEquals(match.getAwayTeamGoals(homeTeam), 2);
		Assert.assertEquals(match.getAwayTeamGoals(awayTeam), 2);
	}

//	Match match1 = new Match("france","swiss", 2, 2);
//	Match match2 = new Match("france","danemark", 2, 1);

}
