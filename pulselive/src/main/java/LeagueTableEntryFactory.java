import com.sun.istack.internal.NotNull;

public class LeagueTableEntryFactory {

	public static LeagueTableEntry createLeagueTableEntry(@NotNull Match match, @NotNull String team){

		int teamGoals = match.getHomeTeamGoals(team);
		int awayTeamGoals = match.getAwayTeamGoals(team);

		int goalDifference = teamGoals - awayTeamGoals;
		int points = match.getPoint(team);
		int won = match.hasWon(team) ? 1 : 0;
		int lost = match.hasLost(team) ? 1 : 0;
		int drawn = match.isDraw() ? 1 : 0;

		return new LeagueTableEntry(team, 1, won, drawn, lost, teamGoals, awayTeamGoals, goalDifference, points);

	}






}
