import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LeagueTable {

	private List<Match> matches;

	public LeagueTable(final List<Match> matches){

		this.matches = matches;
	}

	public List<LeagueTableEntry> getTableEntries(){

		return getMapEntries().values().stream().sorted(Comparator.comparingInt(LeagueTableEntry::getPoints).thenComparingInt(LeagueTableEntry::getGoalsFor).reversed()).collect(Collectors.toList());
	}


	public Map<String, LeagueTableEntry> getMapEntries(){

		Map<String, LeagueTableEntry> result = new HashMap<>();

		if(matches != null){
			for(int i = 0;  i < matches.size() ; i++){

				Match match = matches.get(i);

				LeagueTableEntry homeLeagueTableEntry = result.get(match.getHomeTeam());

				updateEntryMap(result, match, homeLeagueTableEntry, match.getHomeTeam());

				LeagueTableEntry awayLeagueTableEntry = result.get(match.getAwayTeam());

				updateEntryMap(result, match, awayLeagueTableEntry, match.getAwayTeam());
			}
		}

		return result;
	}

	private void updateEntryMap(Map<String, LeagueTableEntry> result, Match match, LeagueTableEntry leagueTableEntry, String teamName) {
		if (leagueTableEntry == null) {
			result.put(teamName, LeagueTableEntryFactory.createLeagueTableEntry(match, teamName));

		} else {
			leagueTableEntry.modifyLeagueTableEntry(match, teamName);
		}
	}

}
