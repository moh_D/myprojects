import java.util.ArrayList;
import java.util.List;


public class Main {

	public static void main( String [] args){

		Match match  = new Match("france","england", 2, 0);
		Match match1 = new Match("france","swiss", 2, 2);

		Match match2 = new Match("france","danemark", 2, 1);

		Match match3 = new Match("england","swiss", 2, 0);

		Match match4 = new Match("england","danemark", 2, 2);

		Match match5 = new Match("danemark","swiss", 2, 2);


		List<Match> matches = new ArrayList<>();
		matches.add(match);
		matches.add(match1);
		matches.add(match2);
		matches.add(match3);
		matches.add(match4);
		matches.add(match5);



		LeagueTable leagueTable = new LeagueTable(matches);

		List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntries();

		for (LeagueTableEntry leagueTableEntry: leagueTableEntries) {
			System.out.println(leagueTableEntry.toString());
		}
	}
}
