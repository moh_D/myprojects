public class Match {

	private String homeTeam;
	private String awayTeam;
	private int homeScore;
	private int awayScore;

	public Match( final String homeTeam, final String awayTeam, final int homeScore, final int awayScore){
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.homeScore = homeScore;
		this.awayScore = awayScore;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public String getAwayTeam() {
		return awayTeam;
	}

	public int getHomeScore() {
		return homeScore;
	}

	public int getAwayScore() {
		return awayScore;
	}

	public boolean hasWon(String team){

		if(homeTeam.equals(team)){
			return homeScore > awayScore;
		}
		if(awayTeam.equals(team)){
			return awayScore > homeScore;
		}
		return false;
	}

	public boolean hasLost(String team){

		if(homeTeam.equals(team)){
			return homeScore < awayScore;
		}
		if(awayTeam.equals(team)){
			return awayScore < homeScore;
		}
		return false;
	}

	public boolean isDraw(){

		return homeScore == awayScore;
	}

	public int getPoint(String team){

		if(hasWon(team)){
			return 3;
		}
		if(isDraw()){
			return 1;
		}
		if(hasLost(team)){
			return 0;
		}
		return 0;
	}

	public int getHomeTeamGoals(String team){
		if (homeTeam.equals(team)) {
			return homeScore;
		}
		return awayScore;
	}

	public  int getAwayTeamGoals(String team){
		if(!awayTeam.equals(team)) {
			return awayScore;
		}
		return homeScore;
	}

}
