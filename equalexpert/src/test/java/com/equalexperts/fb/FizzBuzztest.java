package com.equalexperts.fb;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class FizzBuzztest{

	Fizzbuzz fizzbuzz;

	@Before
	public void setUp(){

		fizzbuzz = new Fizzbuzz();
	}

	@Test
	public void fizzBuzz_inputMultipleof3_ReturnFizz() {
		Assert.assertEquals("fizz", fizzbuzz.fizzOrBuzz(3));
	}

	@Test
	public void fizzBuzz_inputMultipleOf5_ReturnBuzz() {
		Assert.assertEquals("buzz", fizzbuzz.fizzOrBuzz(5));
	}

	@Test
	public void fizzBuzz_inputMultpipleOf3and5_ReturnFizzBuzz() {
		Assert.assertEquals("fizzbuzz", fizzbuzz.fizzOrBuzz(15));
	}

	@Test
	public void fizzBuzz_inputNotMultipleOf3or5_ReturnInput() {
		Assert.assertEquals("4", fizzbuzz.fizzOrBuzz(4));
	}

	@Test
	public void fizzBuzz_inputNotmultipleOf3or5_ReturnInput() {
		Assert.assertEquals("23", fizzbuzz.fizzOrBuzz(23));
	}


	@Test
	public void containsNumberThree_Input3_ReturnTrue(){
		Assert.assertTrue(fizzbuzz.containsNumberThree(3));
	}

	@Test
	public void containsNumberThree_Input13_ReturnTrue(){
		Assert.assertTrue(fizzbuzz.containsNumberThree(13));
	}

	@Test
	public void containsNumberThree_Input33_ReturnTrue(){
		Assert.assertTrue(fizzbuzz.containsNumberThree(33));
	}

	@Test
	public void containsNumberThree_Input5_ReturnFalse(){
		Assert.assertFalse(fizzbuzz.containsNumberThree(5));
	}

	@Test
	public void containsNumberThree_Input1_ReturnFalse(){
		Assert.assertFalse(fizzbuzz.containsNumberThree(1));
	}

	@Test
	public void containsNumberThree_Input10_ReturnFalse(){
		Assert.assertFalse(fizzbuzz.containsNumberThree(10));
	}

	@Test
	public void getFrequencyTest_Input20_ReturnTheRightResult() {

		List<String> result1 = fizzbuzz.resultList(20);

		Assert.assertEquals(4, fizzbuzz.getFrequency(result1, "fizz"));
		Assert.assertEquals(2, fizzbuzz.getFrequency(result1, "lucky"));
		Assert.assertEquals(1, fizzbuzz.getFrequency(result1, "fizzbuzz"));
		Assert.assertEquals(3, fizzbuzz.getFrequency(result1, "buzz"));
	}

	@Test
	public void getFrequencyTest_Input15_ReturnTheRightResult() {
		List<String> result2 = fizzbuzz.resultList(15);

		Assert.assertEquals(3, fizzbuzz.getFrequency(result2, "fizz"));
		Assert.assertEquals(2, fizzbuzz.getFrequency(result2, "lucky"));
		Assert.assertEquals(1, fizzbuzz.getFrequency(result2, "fizzbuzz"));
		Assert.assertEquals(2, fizzbuzz.getFrequency(result2, "buzz"));
	}

	@Test
	public void getFrequencyTest_Input9_ReturnTheRightResult() {
		List<String> result3 = fizzbuzz.resultList(9);

		Assert.assertEquals(2, fizzbuzz.getFrequency(result3, "fizz"));
		Assert.assertEquals(1, fizzbuzz.getFrequency(result3, "lucky"));
		Assert.assertEquals(0, fizzbuzz.getFrequency(result3, "fizzbuzz"));
		Assert.assertEquals(1, fizzbuzz.getFrequency(result3, "buzz"));
	}

	@Test
	public void getFrequencyTest_Input0_ReturnTheRightResult(){
		List<String> result4 = fizzbuzz.resultList(0);

		Assert.assertEquals(0, fizzbuzz.getFrequency(result4, "fizz"));
		Assert.assertEquals(0, fizzbuzz.getFrequency(result4, "lucky"));
		Assert.assertEquals(0, fizzbuzz.getFrequency(result4, "fizzbuzz"));
		Assert.assertEquals(0, fizzbuzz.getFrequency(result4, "buzz"));
	}


	@Test
	public void resultListTest_input0_ListIsEmpty() {
		Assert.assertEquals(0, fizzbuzz.resultList(0).size());
		Assert.assertTrue(fizzbuzz.resultList(0).isEmpty());
	}

	@Test
	public void resultListTest_input5_ListIsNotEmptyAndExpectingRightResult() {
		Assert.assertEquals(5, fizzbuzz.resultList(5).size());
		Assert.assertTrue(fizzbuzz.resultList(5).contains("lucky"));
		Assert.assertTrue(fizzbuzz.resultList(5).contains("buzz"));
		Assert.assertFalse(fizzbuzz.resultList(5).contains("fizz"));
		Assert.assertFalse(fizzbuzz.resultList(5).contains("fizzbuzz"));
	}

	@Test
	public void resultListTest_input15_ListIsNotEmptyAndExpectingRightResult(){
		Assert.assertEquals(15,fizzbuzz.resultList(15).size());

		Assert.assertTrue(fizzbuzz.resultList(15).contains("lucky"));
		Assert.assertTrue(fizzbuzz.resultList(15).contains("fizz"));
		Assert.assertTrue(fizzbuzz.resultList(15).contains("buzz"));
		Assert.assertTrue(fizzbuzz.resultList(15).contains("fizzbuzz"));
	}


}
