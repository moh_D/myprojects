package com.equalexperts.fb;

import java.util.List;

public class Main {

	public static void main(String [] args){

		int numberToTest = 3;
		Fizzbuzz fizzbuzz = new Fizzbuzz();

		List<String> result = fizzbuzz.resultList(numberToTest);

		fizzbuzz.printResult(result);
	}
}
