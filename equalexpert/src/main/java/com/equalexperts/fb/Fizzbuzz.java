package com.equalexperts.fb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Fizzbuzz {

	private final String LUCKY_NUMBER = "3";
	private final String FIZZ = "fizz";
	private final String BUZZ = "buzz";
	private final String FIZZBUZZ = "fizzbuzz";
	private final String LUCKY = "lucky";
	private final List<String>  WORDS_TO_PRINT = new ArrayList<>(Arrays.asList(FIZZ,BUZZ,FIZZBUZZ,LUCKY));


	public String fizzOrBuzz(int multiple){

		if ((multiple % 3 == 0) && (multiple % 5 == 0)) {
			return FIZZBUZZ;
		}
		if (multiple % 3 == 0) {
			return FIZZ;
		}
		if (multiple % 5 == 0) {
			return BUZZ;
		}
		else {
			return String.valueOf(multiple);
		}
	}

	public boolean containsNumberThree(int number){
		return String.valueOf(number).contains(LUCKY_NUMBER);
	}

	public List<String> resultList(int rangeNumber){

		List<String> list = new ArrayList<>(rangeNumber);

		for (int i = 1; i <= rangeNumber; i++) {

			if (containsNumberThree(i)) {
				list.add(LUCKY);

			} else {
				list.add(fizzOrBuzz(i));
			}
		}

		return list;
	}

	public int getFrequency(List<String> resultList, String word) {

		int frequency = Collections.frequency(resultList, word);

		return frequency;
	}

	public List<String> statistics(List<String> list){

		List<String> statisticsList = new ArrayList<>();
		int wordsCount = 0;

		for (int i = 0; i < WORDS_TO_PRINT.size() ; i++) {
			int frequency = getFrequency(list, WORDS_TO_PRINT.get(i));

			statisticsList.add(WORDS_TO_PRINT.get(i) + ": " + frequency);

			wordsCount = wordsCount + frequency;
		}

		statisticsList.add("integer: " + (list.size() - wordsCount));

		return statisticsList;
	}

	public void printResult(List<String> list){

		list.addAll(statistics(list));

		String result = String.join(" ", list);

		System.out.println(result);
	}

}
